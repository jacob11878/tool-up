//
//  Details.swift
//  ToolUp!
//
//  Created by Julka on 20.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit
class Details: UIViewController {
    @IBOutlet var nameField: UITextField!
    @IBOutlet var lengthField: UITextField!
    @IBOutlet var widthField: UITextField!
    @IBOutlet var heightField: UITextField!
    @IBOutlet var dateLabel: UILabel!
    
    var object: Object!
    let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameField.text = object.name
        //lengthField.text = "\(String(describing: object.objLength))"
        lengthField.text = numberFormatter.string(from: NSNumber(value: object.objLength!))
        widthField.text = numberFormatter.string(from: NSNumber(value: object.objWidth!))
        heightField.text = numberFormatter.string(from: NSNumber(value: object.objHeight!))
        //widthField.text = "\(String(describing: object.objWidth))"
        //heightField.text = "\(String(describing: object.objHeight))"
        dateLabel.text = dateFormatter.string(from: object.dateCreated)
    }
}
