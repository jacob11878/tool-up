//
//  Volume_2.swift
//  ToolUp!
//
//  Created by Julka on 24.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit

class Volume_2: UIViewController {
    
    @IBOutlet var resultVolumeLabel2: UILabel!
    @IBOutlet var textFieldR: UITextField!
    @IBOutlet var textFieldH2: UITextField!
    var radiusValue: Double? {
        var radius = Double(textFieldR.text!)
        return radius
    }
    var height2Value: Double? {
        var height = Double(textFieldH2.text!)
        return height
    }
    
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 2
        return nf
    }()
    
    @IBAction func dismissKeyboard2(_ sender: UITapGestureRecognizer) {
        textFieldR.resignFirstResponder()
        textFieldH2.resignFirstResponder()
    }

    var cylinderVolume: Double? {
        if let CH = height2Value, let CR = radiusValue {
            var cylinderVolume1 = .pi*CR*CR*CH
            return cylinderVolume1
        } else {
            return nil
            //let errorString = "Instrument reported lack of a value , please use all required values."
        }
    }
    
    func updateResultLabel2 () {
        if let cylinderVolume = cylinderVolume {
            resultVolumeLabel2.text = numberFormatter.string(from: NSNumber(value: cylinderVolume))
        } else {
            resultVolumeLabel2.text = "???"
        }
    }

    @IBAction func computeTapped2(_ button: UIButton) {
        updateResultLabel2()
    }
}
