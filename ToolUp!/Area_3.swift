//
//  Area_3.swift
//  ToolUp!
//
//  Created by Julka on 24.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit

class Area_3: UIViewController {
    
    @IBOutlet var resultAreaLabel3: UILabel!
    @IBOutlet var textFieldWA3: UITextField!
    @IBOutlet var textFieldHA3: UITextField!
    
    var widthValueA3: Double? {
        var widthA3 = Double(textFieldWA3.text!)
        return widthA3
    }
    var heightValueA3: Double?{
        var heightA3 = Double(textFieldHA3.text!)
        return heightA3
    }
    
    @IBAction func dismissKeyboardA3(_ sender: UITapGestureRecognizer) {
        textFieldWA3.resignFirstResponder()
        textFieldHA3.resignFirstResponder()
    }
    
    var rectangleArea: Double? {
        if let WA3 = widthValueA3, let HA3 = heightValueA3 {
            var rectangleArea1 = WA3*HA3
            return rectangleArea1
        } else {
            return nil
            //let errorString = "Instrument reported lack of a value , please use all required values."
        }
    }
    
    func updateResultALabel3 () {
        if let rectangleArea = rectangleArea {
            resultAreaLabel3.text = "\(rectangleArea)"
        } else {
            resultAreaLabel3.text = "???"
        }
    }
    @IBAction func computeTappedA3(_ button: UIButton) {
        updateResultALabel3()
    }
}
