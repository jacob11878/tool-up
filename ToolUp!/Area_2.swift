//
//  Area_2.swift
//  ToolUp!
//
//  Created by Julka on 24.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit

class Area_2: UIViewController {
    @IBOutlet var resultAreaLabel2: UILabel!
    @IBOutlet var textFieldRA: UITextField!
    @IBOutlet var textFieldH2A: UITextField!
    
    var radiusValueA: Double? {
        var radiusA = Double(textFieldRA.text!)
        return radiusA
    }
    var heightValue2A: Double?{
        var height2A = Double(textFieldH2A.text!)
        return height2A
    }
    
    let numberFormatter2: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 2
        return nf
    }()
    
    @IBAction func dismissKeyboardA(_ sender: UITapGestureRecognizer) {
        textFieldRA.resignFirstResponder()
        textFieldH2A.resignFirstResponder()
    }
    
    
    var cylinderArea: Double? {
        if let CHA = heightValue2A, let CRA = radiusValueA {
            var cylinderArea1 = 2*((.pi*CRA*CRA)+(.pi*CRA*CHA))
            return cylinderArea1
        } else {
            return nil
            //let errorString = "Instrument reported lack of a value , please use all required values."
        }
    }
    
    func updateResultALabel2 () {
        if let cylinderArea = cylinderArea {
            resultAreaLabel2.text = numberFormatter2.string(from: NSNumber(value: cylinderArea))
        } else {
            resultAreaLabel2.text = "???"
        }
    }
    @IBAction func computeTappedA2(_ button: UIButton) {
        updateResultALabel2()
}

}
