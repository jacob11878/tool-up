//
//  Volume.swift
//  ToolUp!
//
//  Created by Julka on 24.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit

class Volume: UIViewController {
    
    @IBOutlet var resultVolumeLabel: UILabel!
    @IBOutlet var textFieldL: UITextField!
    @IBOutlet var textFieldW: UITextField!
    @IBOutlet var textFieldH: UITextField!
    
    var lenghtValue: Double? {
        var lenght = Double(textFieldL.text!)
        return lenght
    }
    var widthValue: Double? {
        var width = Double(textFieldW.text!)
        return width
    }
    var heightValue: Double?{
        var height = Double(textFieldH.text!)
        return height
    }
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        textFieldL.resignFirstResponder()
        textFieldW.resignFirstResponder()
        textFieldH.resignFirstResponder()
    }
    
    
    var cuboidVolume: Double? {
        if let L = lenghtValue, let W = widthValue, let H = heightValue {
            var cuboidVolume1 = L*W*H
            return cuboidVolume1
        } else {
            return nil
            //let errorString = "Instrument reported lack of a value , please use all required values."
        }
    }
    
    func updateResultLabel () {
        if let cuboidVolume = cuboidVolume {
            resultVolumeLabel.text = "\(cuboidVolume)"
        } else {
            resultVolumeLabel.text = "???"
        }
    }
    @IBAction func computeTapped(_ button: UIButton) {
      updateResultLabel()
    }
}
