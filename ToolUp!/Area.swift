//
//  Area.swift
//  ToolUp!
//
//  Created by Julka on 20.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit

class Area: UIViewController {
    
    @IBOutlet var resultAreaLabel: UILabel!
    @IBOutlet var textFieldLA: UITextField!
    @IBOutlet var textFieldWA: UITextField!
    @IBOutlet var textFieldHA: UITextField!
    
    var lenghtValueA: Double? {
        var lenghtA = Double(textFieldLA.text!)
        return lenghtA
    }
    var widthValueA: Double? {
        var widthA = Double(textFieldWA.text!)
        return widthA
    }
    var heightValueA: Double?{
        var heightA = Double(textFieldHA.text!)
        return heightA
    }
    
    @IBAction func dismissKeyboardA(_ sender: UITapGestureRecognizer) {
        textFieldLA.resignFirstResponder()
        textFieldWA.resignFirstResponder()
        textFieldHA.resignFirstResponder()
    }
    
    
    var cuboidArea: Double? {
        if let LA = lenghtValueA, let WA = widthValueA, let HA = heightValueA {
            var cuboidArea1 = 2*((WA*HA)+(LA*WA)+(LA*HA))
            return cuboidArea1
        } else {
            return nil
            //let errorString = "Instrument reported lack of a value , please use all required values."
        }
    }
    
    func updateResultALabel () {
        if let cuboidArea = cuboidArea {
            resultAreaLabel.text = "\(cuboidArea)"
        } else {
            resultAreaLabel.text = "???"
        }
    }
    @IBAction func computeTappedA(_ button: UIButton) {
        updateResultALabel()
    }
}
