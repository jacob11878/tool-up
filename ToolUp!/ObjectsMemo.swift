//
//  ObjectsMemo.swift
//  ToolUp!
//
//  Created by Julka on 20.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit

class ObjectsMemo {
    
    var allObjects = [Object]()
    
    @discardableResult func createObject() -> Object {
        let newObject = Object(random: true)
        allObjects.append(newObject)
        return newObject
    }
    
    func removeObject(_ object: Object) {
        if let index = allObjects.index(of: object) {
            allObjects.remove(at: index)
        }
    }
    
    func moveObject(from fromIndex: Int, to toIndex: Int) {
        if fromIndex == toIndex {
            return }
        // Get reference to object being moved so you can reinsert it
        let movedObject = allObjects[fromIndex]
        // Remove item from array
        allObjects.remove(at: fromIndex)
        // Insert item in array at new location
        allObjects.insert(movedObject, at: toIndex)
    }
}
