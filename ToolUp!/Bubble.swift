//
//  Bubble.swift
//  ToolUp!
//
//  Created by Julka on 20.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit
import CoreMotion

class Bubble: UIViewController {
    var motionManager = CMMotionManager()
    
    @IBOutlet var progressViewZ: UIProgressView!
    @IBOutlet var perfectLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool)
    {
        if motionManager.isGyroAvailable {
        motionManager.gyroUpdateInterval = 1.0/0.5 /* Hz*/
        
        motionManager.startGyroUpdates(to: OperationQueue.current!) { (data, error) in
            if let myData = data
            {   print (myData.rotationRate)
                self.progressViewZ.progress = Float(myData.rotationRate.z)
                if myData.rotationRate.z < 0.005, myData.rotationRate.z > -0.005
                {   print("Perfect!")
                    self.perfectLabel.text = "Perfect!"
                }
                else {
                    self.perfectLabel.text = ""
                }
            }
                
        }
        }
    }
}

