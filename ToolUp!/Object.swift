//
//  Objects.swift
//  ToolUp!
//
//  Created by Julka on 20.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit

class Object: NSObject {
    var name: String
    var objLength: Double?
    var objWidth: Double?
    var objHeight: Double?
    let dateCreated: Date
    
    //designated initializer
    init(name: String, objLength: Double?, objWidth: Double?, objHeight: Double?) {
        self.name = name
        self.objLength = objLength
        self.objWidth = objWidth
        self.objHeight = objHeight
        self.dateCreated = Date()
        super.init()
    }
    //dodatkowe
    convenience init(random: Bool = false) {
        if random {
            //let adjectives = ["Fluffy", "Rusty", "Shiny"]
            //let nouns = ["Bear", "Spork", "Mac"]
            //var idx = arc4random_uniform(UInt32(adjectives.count))
            //let randomAdjective = adjectives[Int(idx)]
            //idx = arc4random_uniform(UInt32(nouns.count))
           // let randomNoun = nouns[Int(idx)]
            let randomName = "New object"
            /*let randomValues = [4, 1.5, 2, 6, 0.5, 0.25]
            var idx = arc4random_uniform(UInt32(randomValues.count))
            let randomL = randomValues[Int(idx)]
            let randomW = randomValues[Int(idx)]
            let randomH = randomValues[Int(idx)]*/
            let randomL = 0.0
            let randomW = 0.0
            let randomH = 0.0
            self.init(name: randomName,
                      objLength: randomL, objWidth: randomW, objHeight: randomH)
        } else {
            self.init(name: "", objLength: nil, objWidth: nil, objHeight: nil)
        }
    }
}

