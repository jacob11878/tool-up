//
//  Dane.swift
//  ToolUp!
//
//  Created by Julka on 20.11.2017.
//  Copyright © 2017 Julka. All rights reserved.
//

import UIKit
class Dane: UITableViewController {
    
    var objectsMemo: ObjectsMemo!
    
    @IBAction func addNewObject(_ sender: UIButton) {
        // Create a new item and add it to the store
        let newObject = objectsMemo.createObject()
        // Figure out where that item is in the array
        if let index = objectsMemo.allObjects.index(of: newObject) {
            let indexPath = IndexPath(row: index, section: 0)
            // Insert this new row into the table
            tableView.insertRows(at: [indexPath], with: .automatic)
        }
    }
    @IBAction func toggleEditingMode(_ sender: UIButton) {
        // If you are currently in editing mode...
        if isEditing {
            // Change text of button to inform user of state
            sender.setTitle("Edit", for: .normal)
            // Turn off editing mode
            setEditing(false, animated: true)
        } else {
            // Change text of button to inform user of state
            sender.setTitle("Done", for: .normal)
            // Enter editing mode
            setEditing(true, animated: true)
        }
    }
    

    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return objectsMemo.allObjects.count
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Create an instance of UITableViewCell, with default appearance
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "UITableViewCell")
        // Set the text on the cell with the description of the item
        // that is at the nth index of items, where n = row this cell
        // will appear in on the tableview
        let object = objectsMemo.allObjects[indexPath.row]
        cell.textLabel?.text = object.name
        return cell }
    
    override func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCellEditingStyle,
                            forRowAt indexPath: IndexPath) {
        // If the table view is asking to commit a delete command...
        if editingStyle == .delete {
            let object = objectsMemo.allObjects[indexPath.row]
            // Remove the item from the store
            objectsMemo.removeObject(object)
            // Also remove that row from the table view with an animation
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView,
                            moveRowAt sourceIndexPath: IndexPath,
                            // Update the model
        to destinationIndexPath: IndexPath) {
        objectsMemo.moveObject(from: sourceIndexPath.row, to: destinationIndexPath.row)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "showObject" segue
        switch segue.identifier {
        case "showObject"?:
            // Figure out which row was just tapped
            if let row = tableView.indexPathForSelectedRow?.row {
                // Get the item associated with this row and pass it along
                let object = objectsMemo.allObjects[row]
                let details
                    = segue.destination as! Details
                details.object = object            } default:
                    preconditionFailure("Unexpected segue identifier.")
        }
    }
}
